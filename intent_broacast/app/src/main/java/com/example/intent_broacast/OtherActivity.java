package com.example.intent_broacast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by 俊辉 on 2018/5/23.
 */

public class OtherActivity extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent){
        String value=intent.getStringExtra("testIntent");
        Log.e("IntentReceiver-->Test",value);
    }
}
