package com.example.intent_broacast;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button myButton=null;
    private Button registerButton=null;
    private Button unregisterButton=null;
    private SMSReceiverActivity smsReceiver=null;
    private final String nobody="Who.care.the.name";
    private static final String SMS_ACTION="android.provider.Telephony.SMS_RECEIVED";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myButton=(Button)findViewById(R.id.jumpBtn);
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(nobody);
                intent.putExtra("testIntent","111111");
                sendBroadcast(intent);
            }
        });
        registerButton=(Button)findViewById(R.id.bListener);
        unregisterButton=(Button)findViewById(R.id.unbListener);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                smsReceiver =new SMSReceiverActivity();
                IntentFilter filter=new IntentFilter();
                filter.addAction(SMS_ACTION);
                MainActivity.this.registerReceiver(smsReceiver,filter);
            }
        });
        unregisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.unregisterReceiver(smsReceiver);
            }
        });
    }
}
